COMP_PAPER=completeness.tex

all: completeness

clean:
	rubber --clean $(COMP_PAPER)
	rm -f completeness.pdf

completeness:
	rubber --pdf $(COMP_PAPER)
